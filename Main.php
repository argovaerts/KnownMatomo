<?php

    namespace IdnoPlugins\Matomo {

        use Idno\Common\Plugin;

        class Main extends Plugin {

            function registerPages()
            {

                \Idno\Core\Idno::site()->template()->extendTemplate('account/menu/items', 'matomo/menu');
                \Idno\Core\Idno::site()->routes()->addRoute('account/matomo/?', '\IdnoPlugins\Matomo\Pages\Account');
                \Idno\Core\Idno::site()->template()->extendTemplate('shell/head','matomo/head');

            }

        }

    }