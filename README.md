# Matomo for Known

Matomo for Known  is a helper plugin to add Matomo Analytics to a Known site with ease.

## Installation
The easiest way is to user Composer:
```
composer require argovaerts/known-matomo
```

Alternatively, you can use Git and clone the repository.

```
$ cd Known/IdnoPlugins
$ git clone https://codeberg.org/argovaerts/KnownMatomo.git Matomo
```
Enable the plugin under Site Configuration > Plugins and then go to Account Settings > Matomo to add your Matomo server.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[BlueOak-1.0.0](LICENSE)