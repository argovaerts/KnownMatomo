<?php

    namespace IdnoPlugins\Matomo\Pages {
        use Idno\Common\Page;

        class Account extends Page
        {

            function getContent()
            {
                $this->createGatekeeper();
                $user = \Idno\Core\Idno::site()->session()->currentUser();

                if (!$user->matomoServer) {
                    $user->matomoServer = '';
                }
                if (!$user->matomoSiteId) {
                    $user->matomoSiteId = 1;
                }
                if (!$user->matomoFollowEveryWhere) {
                    $user->matomoFollowEveryWhere = false;
                }
                if (!$user->matomoSubdomainBeforeTitle) {
                    $user->matomoSubdomainBeforeTitle = false;
                }
                if (!$user->matomoHideKnownDomains) {
                    $user->matomoHideKnownDomains = false;
                }

                $t = \Idno\Core\Idno::site()->template();
                $t->body = $t->__($vars)->draw('matomo/account');
                $t->title = 'Matomo';
                $t->drawPage();
            }

            function postContent()
            {
                $this->createGatekeeper();
                $user = \Idno\Core\Idno::site()->session()->currentUser();

                $user->matomoServer                 = trim($this->getInput('matomo_server'));
                $user->matomoSiteId                 = trim($this->getInput('matomo_site_id'));

                if(!is_null($this->getInput('matomo_follow_everywhere')) && $this->getInput('matomo_follow_everywhere') === "on") {
                    $user->matomoFollowEveryWhere = true;
                }
                else {
                    $user->matomoFollowEveryWhere = false;
                }
                if(!is_null($this->getInput('matomo_subdomain_before_title')) && $this->getInput('matomo_subdomain_before_title') === "on") {
                    $user->matomoSubdomainBeforeTitle = true;
                }
                else {
                    $user->matomoSubdomainBeforeTitle = false;
                }
                if(!is_null($this->getInput('matomo_hide_known_domains')) && $this->getInput('matomo_hide_known_domains') === "on") {
                    $user->matomoHideKnownDomains = true;
                }
                else {
                    $user->matomoHideKnownDomains = false;
                }               

                $user->save();

            }
            

        }

    }