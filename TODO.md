- [ ] Install with Composer
  - [ ] Add Composer
  - [ ] Submit to Packigist
- [ ] Add Matomo Privacy options
  - [ ] require consent
  - [ ] user gives consent, with optional duration

```
// require user consent before processing data
_paq.push(['requireConsent']);
_paq.push(['trackPageview']);

_paq.push(['rememberConsentGiven'])
OR
_paq.push(['rememberConsentGiven', optionallyExpireConsentInHours]).

[...]
```

