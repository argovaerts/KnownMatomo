<?php if ($vars['user'] && $vars['user']->matomoServer && $vars['user']->matomoSiteId) { ?>   

<!-- Matomo -->
<script type="text/javascript">
  var _paq = window._paq = window._paq || [];
  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
  
  <?php if ($vars['user']->matomoSubdomainBeforeTitle) { ?> _paq.push(["setDocumentTitle", document.domain + "/" + document.title]); <?php } ?>
  <?php if ($vars['user']->matomoSubdomainBeforeTitle) { ?> _paq.push(["setCookieDomain", "*." + document.domain]); <?php } ?>
  <?php if ($vars['user']->matomoSubdomainBeforeTitle) { ?>  _paq.push(["setDomains", ["*." + document.domain]]); <?php } ?>
 
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//<?= str_replace('//', '/', str_replace('https://', '', str_replace('http://', '', $vars['user']->matomoServer . '/') . '/')) ?>";
    _paq.push(['setTrackerUrl', u+'matomo.php']);
    _paq.push(['setSiteId', <?= $vars['user']->matomoSiteId ?>]);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<!-- End Matomo Code -->

<?php } ?>