<?php
    $user = \Idno\Core\Idno::site()->session()->currentUser();
?>
<div class="row">
    
    <div class="col-md-10 col-md-offset-1">

        <?= $this->draw('account/menu') ?>
        <h1>Matomo</h1>

    </div>

    <div class="col-md-10 col-md-offset-1">
        <form method="post" class="navbar-form admin">
            <div class="row">
                <div class="col-md-2">
                    <label class="control-label" for="matomo_server">Matomo server</label>
                </div>
                <div class="col-md-6">
                    <input type="text" name="matomo_server" id="matomo_server"
                        value="<?= $user->matomoServer ? $user->matomoServer : '' ?>" placeholder="https://example.com" />
                </div>
            </div>

            <div class="row">
                <div class="col-md-2">
                    <label class="control-label" for="matomo_site_id">Matomo site ID</label>
                </div>
                <div class="col-md-6">
                    <input type="text" name="matomo_site_id" id="matomo_site_id"
                        value="<?= $user->matomoSiteId ? $user->matomoSiteId : '' ?>" placeholder="1" />
                </div>
            </div>

            <div class="row">
                <div class="col-md-8 d-inline">
                    <?php
                    if($user->matomoFollowEveryWhere) {
                        echo '<input type="checkbox" class="form-check-input" name="matomo_follow_everywhere" id="matomo_follow_everywhere" checked="checked">';
                    }
                    else {
                        echo '<input type="checkbox" class="form-check-input" name="matomo_follow_everywhere" id="matomo_follow_everywhere">';
                    }
                    ?>
                    <label class="form-check-label" for="matomo_follow_everywhere">Follow visitors on every subdomain (requires Known to be installed on apex domain)</label>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8 d-inline">
                    <?php
                    if($user->matomoSubdomainBeforeTitle) {
                        echo '<input type="checkbox" class="form-check-input" name="matomo_subdomain_before_title" id="matomo_subdomain_before_title" checked="checked">';
                    }
                    else {
                        echo '<input type="checkbox" class="form-check-input" name="matomo_subdomain_before_title" id="matomo_subdomain_before_title">';
                    }
                    ?>
                    <label class="form-check-label" for="matomo_subdomain_before_title">Put subdomain before page title in tracking report</label>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8 d-inline">
                    <?php
                    if($user->matomoHideKnownDomains) {
                        echo '<input type="checkbox" class="form-check-input" name="matomo_hide_known_domains" id="matomo_hide_known_domains" checked="checked">';
                    }
                    else {
                        echo '<input type="checkbox" class="form-check-input" name="matomo_hide_known_domains" id="matomo_hide_known_domains">';
                    }
                    ?>
                    <label class="form-check-label" for="matomo_hide_known_domains">Hide clicks to known aliases in the Outgoing links report</label>
                </div>
            </div>

            <div class="controls-save">
                <button type="submit" class="btn btn-primary">Save updates</button>
            </div>

            <?= \Idno\Core\Idno::site()->actions()->signForm('/account/matomo') ?>
        </form>
    </div>

</div>